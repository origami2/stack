var Stack = require('../stack');
var assert = require('assert');
var testData = require('./encryptionTest');
var crypto = require('origami-crypto-utils');
var ConnectedEmitters = require('origami-connected-emitters');

function et2s(what) {
  return crypto
    .encryptAndSign(
      what,
      testData.tokenProvider.privateKey,
      testData.stack.publicKey
    );
}

function dt2s(what) {
  return crypto
    .decryptAndVerify(
      what,
      testData.stack.privateKey,
      testData.tokenProvider.publicKey
    );
}

var stackToken = crypto.encryptAndSign(
  {
    username: 'myuser',
    fold: 'fold1'
  },
  testData.stack.privateKey,
  testData.stack.publicKey
);

describe('bus2bus', function () {
  var target;
  var pluginBus1;
  var pluginBus2;
  var pluginBus1be;
  var pluginBus2be;
  var tokenProviderBus;
  var clientBus;

  describe('client2plugin', function () {
    beforeEach(function () {
      target = new Stack(testData.stack.privateKey, testData.stack.publicKey);
      
      var pluginBus1CE = new ConnectedEmitters();
      var pluginBus2CE = new ConnectedEmitters();
      var tokenProviderBusCE = new ConnectedEmitters();
      var clientBusCE = new ConnectedEmitters();
      
      pluginBus1 = pluginBus1CE.createEmitter();
      pluginBus2 = pluginBus2CE.createEmitter();
      clientBus = clientBusCE.createEmitter();
      tokenProviderBus = tokenProviderBusCE.createEmitter();

      target.addPlugin('StubPlugin1', (pluginBus1be = pluginBus1CE.createEmitter()), {echo:['what']});
      target.addPlugin('StubPlugin2', (pluginBus2be = pluginBus2CE.createEmitter()), {echo:['what']});
      target.listenSocket(clientBusCE.createEmitter());
      target.addTokenProvider('TokenProvider1', tokenProviderBusCE.createEmitter(), {}, testData.tokenProvider.publicKey);
    });

    describe('form client', function () {
      describe('api', function () {
        it('passes call method', function (done) {
          pluginBus1
          .on(
            'api',
            function (token, context, methodName, params, callback) {
              try {
                assert.equal(token, stackToken);
                assert.deepEqual({
                  fold: 'fold1',
                  username: 'myuser'
                }, context);
                assert.equal('echo', methodName);
                assert.deepEqual({
                  'what': 'hello'
                }, params);

                done();
              } catch (e) {
                done(e);
              }
            }
          );

          clientBus
          .emit(
            'api',
            stackToken,
            'StubPlugin1',
            'echo',
            { 'what': 'hello' },
            function (err, result) {
            }
          );
        });

        it('answers successful call', function (done) {
          pluginBus1
          .on(
            'api',
            function (
              token, pluginName, methodName, params, callback) {
              callback(null, params.what);
            }
          );

          clientBus
          .emit(
            'api',
            stackToken,
            'StubPlugin1',
            'echo',
            { 'what': 'hello' },
            function (err, result) {
              try {
                assert.equal('hello', result);

                done();
              } catch (e) {
                done(e);
              }
            }
          );
        });

        it('answers unsuccessful call', function (done) {
          pluginBus1
          .on(
            'api',
            function (
              token,
              context,
              methodName,
              params,
              callback
            ) {
              callback('my error');
            }
          );

          clientBus
          .emit(
            'api',
            stackToken,
            'StubPlugin1',
            'echo',
            { 'what': 'hello' },
            function (err, result) {
              try {
                assert.equal('my error', err);

                done();
              } catch (e) {
                done(e);
              }
            }
          );
        });

        it('rejects non-existent plugin', function (done) {
          clientBus
          .emit(
            'api',
            stackToken,
            'IDontExists',
            'echo',
            { 'what': 'hello' },
            function (err, result) {
              try {
                assert.equal('invalid plugin', err);

                done();
              } catch (e) {
                done(e);
              }
            }
          );
        });

        it('rejects non-existent method', function (done) {
          clientBus
          .emit(
            'api',
            stackToken,
            'StubPlugin1',
            'IDontExists',
            { 'what': 'hello' },
            function (err, result) {
              try {
                assert.equal('invalid method', err);

                done();
              } catch (e) {
                done(e);
              }
            }
          );
        });
      });

      describe('describe-apis', function () {
        it('returns existing plugins with methods', function (done) {
          clientBus
          .emit(
            'describe-apis',
            function (err, apis) {
              assert.deepEqual({
                'StubPlugin1': {
                  echo: ['what']
                },
                'StubPlugin2': {
                  echo: ['what']
                }
              }, apis);

              done();
            }
          );
        });
      });

      describe('describe-api', function () {
        it('returns method for existing plugin', function (done) {
          clientBus
          .emit(
            'describe-api',
            'StubPlugin1',
            function (err, methods) {
              assert.deepEqual({
                echo: ['what']
              }, methods);

              done();
            }
          );
        });

        it('returns Plugin unavailable for non-existent plugins', function (done) {
          clientBus.emit(
            'describe-api',
            'DoesNotExists',
            function (err, methods) {
              try {
                assert.equal('Plugin unavailable', err);

                done();
              } catch (e) {
                done(e);
              }
            }
          );
        });
      });
    });
  });
})
