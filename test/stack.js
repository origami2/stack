/* global context */

var Stack = require('../stack');
var EventEmitter = require('events').EventEmitter;
var assert = require('assert');
var testData = require('./encryptionTest');
var crypto = require('origami-crypto-utils');
var jsHelpers = require('origami-js-function-helpers');

function et2s(what) {
  return crypto
    .encryptAndSign(
      what,
      testData.tokenProvider.privateKey,
      testData.stack.publicKey
    );
}

var stackToken = crypto.encryptAndSign(
  {
    'fold': 'fold1',
    'username': 'myuser'
  },
  testData.stack.privateKey,
  testData.stack.publicKey
);

describe('stack', function () {
  var target;

  it('requires private key to instantiate', function () {
    assert.throws(
      function () {
        new Stack();
      },
      /private key is required/
    );
  });

  beforeEach(function () {
    target = new Stack(
      testData.stack.privateKey,
      testData.stack.publicKey
    );
  });

  describe('.getPublicKey', function () {
    it('returns public key', function () {
      assert.equal(testData.stack.publicKey, target.getPublicKey());
    });
  });

  describe('.getPrivateKey', function () {
    it('returns public key', function () {
      assert.equal(testData.stack.privateKey, target.getPrivateKey());
    });
  });

  it('lists no subscribers at first', function () {
    assert.deepEqual([], target.listSubscribers('some.change'));
  });

  it('lists no guards at first', function () {
    assert.deepEqual([], target.listGuards());
  });

  it('lists no token providers at first', function () {
    assert.deepEqual([], target.listTokenProviders());
  });

  var fakeSubscriber;
  
  beforeEach(function() {
    fakeSubscriber = new EventEmitter();
  });

  describe('.addSubscriber', function () {
    it('adds a subscriber', function () {
      target.addSubscriber(fakeSubscriber, 'some.change');
      
      assert
      .deepEqual(
        [fakeSubscriber],
        target.listSubscribers('some.change')
      );
    });
  });

  describe('.publish', function () {
    it('publishes to subscribers', function (done) {
      target.addSubscriber(fakeSubscriber, 'some.change');

      fakeSubscriber
      .on('event', function (namespace, token, message) {
        try {
          assert.equal('some.change', namespace);
          assert.deepEqual({ theToken: true }, token);
          assert.deepEqual({
            magic: 'thing'
          }, message);
          
          done();
        } catch (e) {
          done(e);
        }
      });

      target.publishEvent('some.change', { theToken: true }, { magic: 'thing' });
    });
    
    it('uses socket filter publishes to subscribers', function (done) {
      target.addSubscriber(fakeSubscriber, 'some.change');

      fakeSubscriber
      .on('event', function (namespace, token, message) {
        done('this shouldn\'t have happened');
      });

      target
      .publishEvent(
        'some.change',
        { theToken: true },
        { magic: 'thing' },
        function () {
          return false;
        }
      );
      
      done();
    });

    it('double addSubscriber to not publishes twice', function () {
      target.addSubscriber(fakeSubscriber, 'some.change');
      target.addSubscriber(fakeSubscriber, 'some.change');

      var times = 0;
      fakeSubscriber
      .on('event', function (namespace, token, message) {
        times++;
      });

      target.publishEvent('some.change', { theToken: true }, { magic: 'thing' });
      assert.equal(1, times);
    });
    
    it('.removeSubscriber removes subscriber', function () {
      var fakeSubscriber2 = new EventEmitter();
      
      target.addSubscriber(fakeSubscriber, 'some.change');
      target.addSubscriber(fakeSubscriber2, 'some.change');

      var called1;
      var called2;
      
      fakeSubscriber
      .on('event', function (namespace, token, message) {
        called1 = true;
      });
      
      fakeSubscriber2
      .on('event', function (namespace, token, message) {
        called2 = true;
      });

      target.removeSubscriber(fakeSubscriber2, 'some.change');
      target.publishEvent('some.change', { theToken: true }, { magic: 'thing' });
      
      assert(called1);
      assert(!called2);
    });
    
    context('after socket disconnection', function () {
      it('skips disconnected socket', function (done) {
        target.addSubscriber(fakeSubscriber, 'some.change');
      
        fakeSubscriber
        .on('event', function (namespace, token, message) {
          done(new Error('this shouldn\'t have been called'));
        });
        
        fakeSubscriber
        .emit('disconnect');
  
        target.publishEvent('some.change', { theToken: true }, { magic: 'thing' });
        
        done();
      });
    });
  });

  it('lists no plugins at first', function () {
    assert.deepEqual({}, target.listPlugins());
  });

  var stubPlugin1;
  
  beforeEach(function() {
    stubPlugin1 = new EventEmitter();
  });

  describe('.addPlugin', function () {
    it('adds a plugin', function () {
      target.addPlugin('StubPlugin1', stubPlugin1, {});

      assert.deepEqual({}, target.listPlugins()['StubPlugin1']);
    });
  });

  describe('.invokeMethod', function () {
    it('invokes method', function () {
      target
      .invokeMethod(
        {
          attr1: 'something'
        },
        {
          fold: 'fold1'
        },
        'StubPlugin1',
        'echo', {
          what: 'this'
        }
      )
      .then(function (result) {
        assert.equal('this', result);
      });
    });
  });

  it('drops a plugin if it disconnects', function (done) {
    stubPlugin1.emit('disconnect');

    assert.deepEqual(undefined, target.listPlugins()['StubPlugin1']);

    done();
  });

  describe('.listenSocket', function () {
    it('emits identify', function (done) {
      var stubSocket = new EventEmitter();

      stubSocket.on('identify', function () {
        done();
      });

      target.listenSocket(stubSocket);
    });

    it('accepts plugin', function (done) {
      var stubPlugin = new EventEmitter();

      stubPlugin.on('identify', function (callback) {
        callback('plugin', 'StubPlugin2', {});

        try {
          assert.deepEqual({}, target.listPlugins()['StubPlugin2']);

          done();
        } catch (e) {
          done(e);
        }
      });

      target.listenSocket(stubPlugin);
    });
  });

  describe('.authorizeGuardPublicKey', function () {
    it('accepts a valid key', function () {
      target
      .authorizeGuardPublicKey('StubGuard1', testData.guard1.publicKey);
      
      assert.deepEqual(['StubGuard1'], target.listGuards());
    });
    
    it('accepts a valid key', function () {
      target
      .authorizeGuardPublicKey('StubGuard1', testData.guard1.publicKey);
      
      assert.deepEqual(['StubGuard1'], target.listGuards());
    });

    it('accepts a valid key', function () {
      assert.throws(
        function () {
          target
          .authorizeGuardPublicKey(
            null,
            testData.guard1.publicKey
          );
        },
        /guard name is required/
      );
    });

    it('rejects an invalid key', function () {
      assert.throws(
        function () {
          target
          .authorizeGuardPublicKey(
            'invalidGuard',
            'invalid key'
          );
        },
        /invalid key/
      );
    });
  });

  describe('.addClient', function () {
    it('requires socket', function () {
      assert
      .throws(
        function () {
          target
          .addClient();
        },
        /socket is required/
      );
    });

    describe('.removeSocket', function () {
      it('removes all listeners', function (done) {
        var stubClient = new EventEmitter();
        
        target
        .addClient(stubClient);
        
        target.removeSocket(stubClient);
        
        try {
          assert.equal(0, stubClient.listeners().length);
          
          done();
        } catch (e) {
          done(e);
        }
      });
    });

    describe('describe-token-providers event', function () {
      it('listents describe-token-providers', function (done) {
        var stubClient = new EventEmitter();
        var stubTokenProvider = new EventEmitter();

        target
        .addTokenProvider(
          'StubTokenProvider1',
          stubTokenProvider,
          {
            'authenticate': ['username', 'password']
          },
          testData.tokenProvider.publicKey
        );

        target
        .addClient(stubClient);

        stubClient
        .emit(
          'describe-token-providers',
          function (err, result) {
            try {
              assert(!err);
              assert.deepEqual(
                {
                  'StubTokenProvider1': {
                    publicKey: testData.tokenProvider.publicKey,
                    methods: {
                      'authenticate': ['username', 'password']
                    }
                  }
                },
                result
              );

              done();
            } catch (e) {
              done(e);
            }
          }
        );
      });
    });

    describe('api event', function () {
      it('listens api', function (done) {
        var stubClient = new EventEmitter();
        var stubPlugin3 = new EventEmitter();

        stubPlugin3
        .on(
          'api',
          function (
            token,
            context,
            method,
            args,
            callback
          ) {
            try {
              assert.equal(token, stackToken);
              assert.deepEqual(
                {
                  'fold': 'fold1',
                  'username': 'myuser'
                },
                context
              );
              assert.equal('echo', method);
              assert.deepEqual(
                {
                  what: 'this'
                },
                args
              );
              assert(callback);

              done();
            } catch (e) {
              done(e);
            }
          }
        );

        target
        .addPlugin(
          'StubPlugin3',
          stubPlugin3,
          {
            'echo': ['what']
          }
        );

        target
        .addClient(stubClient);

        stubClient
        .emit(
          'api',
          stackToken,
          'StubPlugin3',
          'echo',
          {
            what: 'this'
          },
          function (err, result) {

          }
        );
      });

      it('accepts no token, gives no context', function (done) {
          var stubClient = new EventEmitter();
          var stubPlugin3 = new EventEmitter();


          stubPlugin3
          .on(
            'api',
            function (
              token,
              context,
              method,
              args,
              callback
            ) {
              try {
                assert.equal(null, token);
                assert.deepEqual(
                  {
                  },
                  context
                );
                assert.equal('echo', method);
                assert.deepEqual(
                  {
                    what: 'this'
                  },
                  args
                );
                assert(callback);

                done();
              } catch (e) {
                done(e);
              }
            }
          );

          target
          .addPlugin(
            'StubPlugin3',
            stubPlugin3,
            {
              'echo': ['what']
            }
          );

          target
          .addClient(stubClient);

          stubClient
          .emit(
            'api',
            null,
            'StubPlugin3',
            'echo',
            {
              what: 'this'
            },
            function (err, result) {

            }
          );
      });

      it('passes unsuccessful result back', function (done) {
        var stubClient = new EventEmitter();
        var stubPlugin4 = new EventEmitter();

        stubPlugin4
        .on(
          'api',
          function (
            token,
            context,
            method,
            args,
            callback
          ) {
            callback('my error');
          }
        );

        target
        .addPlugin(
          'StubPlugin4',
          stubPlugin4,
          {
            'echo': ['what']
          }
        );

        target
        .addClient(stubClient);
        stubClient
        .emit(
          'api',
          stackToken,
          'StubPlugin4',
          'echo',
          {
            what: 'this'
          },
          function (err, result) {
            try {
              assert.equal('my error', err);

              done();
            } catch (e) {
              done(e);
            }
          }
        );
      });

      it('passes successful result back', function (done) {
        var stubClient = new EventEmitter();
        var stubPlugin5 = new EventEmitter();

        stubPlugin5
        .on(
          'api',
          function (
            token,
            context,
            method,
            args,
            callback
          ) {
            callback(null, 'my result');
          }
        );

        target
        .addPlugin(
          'StubPlugin5',
          stubPlugin5,
          {
            'echo': ['what']
          }
        );

        target
        .addClient(stubClient);
        stubClient
        .emit(
          'api',
          stackToken,
          'StubPlugin5',
          'echo',
          {
            what: 'this'
          },
          function (err, result) {
            try {
              assert.equal('my result', result);

              done();
            } catch (e) {
              done(e);
            }
          }
        );
      });
    });

    describe('get-token event', function () {
      it('passes call', function (done) {
        var stubClient = new EventEmitter();
        var stubTokenProvider = new EventEmitter();

        stubTokenProvider
        .on(
          'get-token',
          function (
            stackToken,
            context,
            methodName,
            params,
            callback
          ) {
            try {
              assert(stackToken === null);
              assert.deepEqual(
                {
                },
                context
              );
              assert.equal('authenticate', methodName);
              assert.deepEqual(
                {
                  username: 'myuser',
                  password: 'secret'
                },
                params
              );

              assert(callback);

              done();
            } catch (e) {
              done(e);
            }
          }
        );

        target
        .addTokenProvider(
          'StubTokenProvider1',
          stubTokenProvider,
          {
            'authenticate': ['username', 'password']
          },
          testData.tokenProvider.publicKey
        );

        target
        .addClient(stubClient);

        stubClient
        .emit(
          'get-token',
          null,
          'StubTokenProvider1',
          'authenticate',
          {
            username: 'myuser',
            password: 'secret'
          },
          function (err, result) {
          }
        );
      });

      it('passes successful response', function (done) {
        var stubClient = new EventEmitter();
        var stubTokenProvider = new EventEmitter();

        stubTokenProvider
        .on(
          'get-token',
          function (
            stackToken,
            context,
            methodName,
            params,
            callback
          ) {
            callback(
              null,
              {
                username: 'myuser',
                permissions: [
                  'somepermission'
                ]
              }
            );
          }
        );

        target
        .addTokenProvider(
          'StubTokenProvider1',
          stubTokenProvider,
          {
            'authenticate': ['username', 'password']
          },
          testData.tokenProvider.publicKey
        );

        target
        .addClient(stubClient);

        stubClient
        .emit(
          'get-token',
          null,
          'StubTokenProvider1',
          'authenticate',
          {
            username: 'myuser',
            password: 'secret'
          },
          function (err, result) {
            try {
              assert(!err);
              assert(result);

              var decrypted = crypto.decryptAndVerify(
                result,
                testData.stack.privateKey,
                testData.stack.publicKey
              );

              assert.deepEqual(
                {
                  username: 'myuser',
                  permissions: [
                    'somepermission'
                  ]
                },
                decrypted
              );

              done();
            } catch (e) {
              done(e);
            }
          }
        );
      });

      it('passes error response', function (done) {
        var stubClient = new EventEmitter();
        var stubTokenProvider = new EventEmitter();

        stubTokenProvider
        .on(
          'get-token',
          function (
            stackToken,
            context,
            methodName,
            params,
            callback
          ) {
            callback(
              'some error'
            );
          }
        );

        target
        .addTokenProvider(
          'StubTokenProvider1',
          stubTokenProvider,
          {
            'authenticate': ['username', 'password']
          },
          testData.tokenProvider.publicKey
        );

        target
        .addClient(stubClient);

        stubClient
        .emit(
          'get-token',
          null,
          'StubTokenProvider1',
          'authenticate',
          {
            username: 'myuser',
            password: 'secret'
          },
          function (err, result) {
            try {
              assert.equal('some error', err);

              done();
            } catch (e) {
              done(e);
            }
          }
        );
      });
    });

    describe('describe-apis event', function () {
      it('listens event', function (done) {
        var stubClient = new EventEmitter();
        var stubPlugin3 = new EventEmitter();

        target
        .addPlugin(
          'StubPlugin3',
          stubPlugin3,
          {
            'echo': ['what']
          }
        );

        target
        .addClient(stubClient);

        stubClient
        .emit(
          'describe-apis',
          function (err, result) {
            try {
              assert(!err);

              assert({
                'StubPlugin3': {
                  'echo': ['what']
                }
              }, result);

              done();
            } catch (e) {
              done(e);
            }
          }
        );
      });
    });
  });

  describe('.addGuardCheck', function () {
    var guardSocket;

    it('adds valid code', function (done) {
      guardSocket = new EventEmitter();

      target.authorizeGuardPublicKey('StubGuard1', testData.guard1.publicKey);
      target.addGuard('StubGuard1', guardSocket);
      
      try {
        function check1(reject) {
          if (fold === 'fold1' && plugin === 'StubPlugin1' && method === 'echo') return;
  
          reject();
        }
  
        target
        .addGuardCheck(
          'StubGuard1',
          'check1',
          jsHelpers.getFunctionBody(check1)
        );
  
        assert.deepEqual({
          'StubGuard1': ['check1']
        }, target.listGuardChecks());
        
        done();
      } catch (e) {
        console.log(e);
        done(e);
      }
    });

    describe('.runCheck', function () {
      it('checks', function (done) {
        guardSocket = new EventEmitter();
  
        target.authorizeGuardPublicKey('StubGuard1', testData.guard1.publicKey);
        target.addGuard('StubGuard1', guardSocket);
        
        function check1(reject) {
          if (fold === 'fold1' && plugin === 'StubPlugin1' && method === 'echo') return;
  
          reject();
        }
  
        target
        .addGuardCheck(
          'StubGuard1',
          'check1',
          jsHelpers.getFunctionBody(check1)
        );
        
        try {
          assert.equal(
            true,
            target
            .runCheck(
              'StubGuard1',
              'check1',
              {
                fold: 'fold1',
                plugin: 'StubPlugin1',
                method: 'echo'
              }
            )
          );
          
          done();
        } catch (e) {
          done(e);
        }
      });

      it('does not check', function (done) {
        guardSocket = new EventEmitter();
  
        target.authorizeGuardPublicKey('StubGuard1', testData.guard1.publicKey);
        target.addGuard('StubGuard1', guardSocket);
        
        function check1(reject) {
          if (fold === 'fold1' && plugin === 'StubPlugin1' && method === 'echo') return;
  
          reject();
        }
  
        target
        .addGuardCheck(
          'StubGuard1',
          'check1',
          jsHelpers.getFunctionBody(check1)
        );
        
        try {
          assert.equal(
            false,
            target
            .runCheck(
              'StubGuard1',
              'check1',
              {
                fold: 'fold2',
                plugin: 'StubPlugin1',
                method: 'echo'
              }
            )
          );
        
          done();
        } catch (e) {
          done(e);
        }
      });

      it('runs checks on .invokeMethod', function (done) {
        var target2 = new Stack(
          testData.stack.privateKey,
          testData.stack.publicKey
        );
        var pluginSocket = new EventEmitter();

        pluginSocket
        .on(
          'api',
          function (token, context, methodName, params, callback) {
            callback(null, 'this');
          }
        );

        var guardSocket = new EventEmitter();
        var check1 = function (reject) {
          if (plugin === 'StubPlugin1' &&
              method === 'echo' &&
              fold === 'fold1') return;

          reject();
        };

        target2.addPlugin('StubPlugin1', pluginSocket, {
          'echo': ['what']
        });
        target2.authorizeGuardPublicKey('guard1', testData.guard1.publicKey);
        target2.addGuard('guard1', guardSocket);
        target2.addGuardCheck('guard1', 'check1', jsHelpers.getFunctionBody(check1));

        target2
        .invokeMethod(
          stackToken,
          {
            fold: 'fold1'
          },
          'StubPlugin1',
          'echo',
          {
            'what': 'this'
          }
        )
        .then(function (result) {
          try {
            assert.equal('this', result);

            target2
            .invokeMethod(
              stackToken,
              {
                fold: 'fold2'
              },
              'StubPlugin1',
              'echo',
              {
                'what': 'this'
              }
            )
            .catch(function (error) {
              assert.equal('not authorized', error);

              done();
            });
         } catch (e) {
            done(e);
          }
        });
      });
    });
  });

  describe('.addGuard', function () {
    var guardSocket;
    
    beforeEach(function () {
      guardSocket = new EventEmitter();
    });

    it('adds guard socket', function () {
      target.authorizeGuardPublicKey('StubGuard1', testData.guard1.publicKey);
      target.addGuard('StubGuard1', guardSocket);

      assert.equal(true, target.isGuardConnected('StubGuard1'));
    });

    it('listens add-check', function () {
      var check1 = (function (reject) {
        if (fold === 'fold2') return reject();
      });

      var code = jsHelpers.getFunctionBody(check1);
      var signed = crypto.encryptAndSign(
          {
            code: code,
            name: 'check1'
          },
          testData.guard1.privateKey,
          testData.stack.publicKey
        );

      guardSocket.emit('add-check', signed, function () {
        assert.deepEqual(
          {
            'StubGuard1': ['check1']
          },
          target.listGuardChecks()
        );
      });
    });

    it('rejects add-check if badly signed', function () {
      var check2 = (function (reject) {
        if (fold === 'fold2') return reject();
      });

      var code = jsHelpers.getFunctionBody(check2);
      var signed = crypto.encryptAndSign(
          {
            code: code,
            name: 'check2'
          },
          testData.guard2.privateKey,
          testData.stack.publicKey
        );

      guardSocket
      .emit('add-check', signed, function (err) {
        assert.equal('invalid signature', err);

        assert.deepEqual(
          {
            'StubGuard1': ['check1']
          },
          target.listGuardChecks()
        );
      });
    });

    it('drops guard on disconnect', function () {
      guardSocket.emit('disconnect');

      assert.equal(false, target.isGuardConnected('StubGuard1'));
    });
  });

  describe('.addTokenProvider', function () {
    it('fails to add it if no socket is provided', function () {
      assert.throws(
        function () {
          target.addTokenProvider(
            'StubTokenProvider1',
            null,
            { 'getToken': [ 'username' ]},
            testData.stack.publicKey
          );
        },
        /socket is required/
      );
    });

    it('fails to add it if no name is provided', function () {
      assert.throws(
        function () {
          target.addTokenProvider(
            null,
            new EventEmitter(),
            { 'getToken': [ 'username' ]},
            testData.stack.publicKey
          );
        },
        /name is required/
      );
    });

    it('fails to add it if no methods is provided', function () {
      assert.throws(
        function () {
          target.addTokenProvider(
            'StubTokenProvider1',
            new EventEmitter(),
            null,
            testData.stack.publicKey
          );
        },
        /methods are required/
      );
    });

    it('fails to add it if no public key is provided', function () {
      assert.throws(
        function () {
          target.addTokenProvider(
            'StubTokenProvider1',
            new EventEmitter(),
            { 'getToken': [ 'username' ]},
            null
          );
        },
        /public key is required/
      );
    });

    it('adds a token provider', function () {
      var stubSocket = new EventEmitter();

      target
      .addTokenProvider(
        'StubTokenProvider1',
        stubSocket,
        { 'getToken': ['username'] },
        testData.tokenProvider.publicKey
      );

      assert.deepEqual(
        {
          'StubTokenProvider1': {'getToken': ['username'] }
        },
        target.listTokenProviders()
      );
    });

    it('verifies token', function () {
      target
      .addTokenProvider(
        'StubTokenProvider1',
        new EventEmitter(),
        { },
        testData.tokenProvider.publicKey
      );
      
      assert.deepEqual(
        {
          username: 'myuser'
        },
        target
        .verifyToken(
          'StubTokenProvider1',
          et2s({
            username: 'myuser'
          })
        )
      );
    });
  });

  describe('.getContextFromToken', function () {
    beforeEach(function () {
      target = new Stack(
        testData.stack.privateKey,
        testData.stack.publicKey
      );
    });

    it('decodes token context', function () {
      var context = target.getContextFromToken(stackToken);

      assert.deepEqual({
        'fold': 'fold1',
        'username': 'myuser'
      }, context);
    });
  });

  describe('.getToken', function () {
    beforeEach(function () {
      target = new Stack(
        testData.stack.privateKey,
        testData.stack.publicKey
      );
    });

    it('passes call to token provider', function (done) {
      var stubSocket = new EventEmitter();

      stubSocket
      .on(
        'get-token',
        function (
          stackToken,
          context,
          methodName,
          params,
          callback
        ) {
          try {
            assert.equal(null, stackToken);
            assert.deepEqual(
              {},
              context
            );
            assert.equal('getToken', methodName);

            assert.deepEqual(
              {
                'username': 'user1'
              },
              params
            );

            assert(callback);

            done();
          } catch (e) {
            done(e);
          }
        }
      );

      target
      .addTokenProvider(
        'StubTokenProvider1',
        stubSocket,
        { 'getToken': ['username'] },
        testData.tokenProvider.publicKey
      );

      target
      .getToken(
        null,
        'StubTokenProvider1',
        'getToken',
        {
          'username': 'user1'
        }
      );
    });
  });
});
