// baptized <Fariña's song>
function RoundRobin(ring) {
  this.ring = ring || [];
  this.lastRun = -1;
}

RoundRobin.prototype.add = function (executor) {
  if (this.ring.indexOf(executor) === -1) this.ring.push(executor);
};

RoundRobin.prototype.remove = function (executor) {
  var index = this.ring.indexOf(executor);

  if (index !== -1) this.ring.splice(index, 1);
};

RoundRobin.prototype.run = function() {
  this.lastRun += 1;
  if (this.lastRun == this.ring.length) this.lastRun = 0;

  var executor = this.ring[this.lastRun];

  if (!executor) throw new Error('Executors list is empty');

  return executor.call(this);
};

module.exports = RoundRobin;
